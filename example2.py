from myoreader.myoReader import MyoReader
from myoreader import *
import datetime
from io import SEEK_SET
import matplotlib.pyplot as plt
import os
import time

#Affichage des signaux de deux stations. Le signal de la station 1, doit être sur la voie 2
#et le signal de la station 2 doit être sur la voie 0.
#Seulent les parties communes (au niveau horraire) des signaux sont affichées afin de 
#permettre la comparaison manuelle de leur phase, amplitude, fréquence etc
#Un troisième graphique montre le psd de la station 1 pour connaitre sa répartition fréquencielle
def main() : 
    path = os.path.dirname(os.path.abspath(__file__)) + "\\Example2\\"
    reader1 = MyoReader(path +"station1.myo")
    reader2 = MyoReader(path + "station2.myo")

    reader1.parse()
    reader2.parse()

    print(reader1.date_time.strftime("%Y/%m/%d %H:%M:%S.%f"))
    print(reader2.date_time.strftime("%Y/%m/%d %H:%M:%S.%f"))

    #delta1 = datetime.timedelta(seconds=reader1.tick_start*reader1.tick_time/1000000)
    #debut1 = reader1.date_time+delta1
    #duree1 = (reader1.tick_start+len(reader1.sensors[2].datas)-1)*reader1.tick_time/1000000
    #delta1 = datetime.timedelta(seconds=duree1)
    #fin1 =  reader1.date_time+delta1
    
    #delta2 = datetime.timedelta(seconds=reader2.tick_start*reader2.tick_time/1000000)
    #debut2 = reader2.date_time+delta2
    #duree2 = (reader2.tick_start+len(reader2.sensors[0].datas)-1)*reader2.tick_time/1000000
    #delta2 = datetime.timedelta(seconds=duree2)
    #fin2 =  reader2.date_time+delta2

    #debut = max(debut1, debut2)
    #fin = min(fin1,fin2)

    #Centralisation des données
    #data1 = copy_data(reader1.sensors[2].datas,debut,fin)
    #data2 = copy_data(reader2.sensors[0].datas,debut,fin)

    #data1 = copy_data(reader1.sensors[2].datas,debut1,fin1)
    #data2 = copy_data(reader2.sensors[0].datas,debut2,fin2)

    data1 = reader1.sensors[0].datas
    data2 = reader2.sensors[0].datas


    ax1 = plt.subplot(3,1,1)
    plt.plot(data1.keys(), data1.values(), 'o-')
    plt.subplot(3,1,2,sharex=ax1)
    plt.plot(data2.keys(), data2.values(), 'o-')
    plt.subplot(3,1,3)
    plt.psd(data2.values(), Fs=1000)
    plt.show()

def copy_data(datas, debut, fin) :
    d = {}
    for k,v in datas.items() :
        if k >= debut and k<= fin :
            d[k] = v

    return d

main()