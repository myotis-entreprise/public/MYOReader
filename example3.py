from myoreader.myoReader import MyoReader
from myoreader import *
import datetime
from io import SEEK_SET
import matplotlib.pyplot as plt
import os
import pandas as pd

def main() : 
    path = os.path.dirname(os.path.abspath(__file__)) + "\\Example3\\"
    id_sensor = 0
    nb_file = 0

    for dir in sorted(os.listdir(path)) :
        nb_file += 1
        print("Fichier " + dir)
        reader1 = MyoReader(path+"/"+dir)
        reader1.parse()
        data1 = reader1.sensors[id_sensor].datas

        #Get the firt point
        date = list(data1.keys())[0]
        nb_errors = 0

        #Check with the previous file
        if nb_file > 1 :
            previous_date += pd.Timedelta(reader1.tick_time*reader1.sensors[id_sensor].saving_tick, unit="ns")
            if date != previous_date :
                print ("Error date " + d.strftime("%Y/%m/%d %H:%M:%S.%f") + " a la place de " + date.strftime("%Y/%m/%d %H:%M:%S.%f"))
                nb_errors += 1
                date = d

        for d in data1.keys() :
            #Compare the theoritical time and the real one in the file
            if date != d :
                print ("Error date " + d.strftime("%Y/%m/%d %H:%M:%S.%f") + " a la place de " + date.strftime("%Y/%m/%d %H:%M:%S.%f"))
                nb_errors += 1
                date = d
            print( d.strftime("%Y/%m/%d %H:%M:%S.%f") + "\r", end="")
            date += pd.Timedelta(reader1.tick_time*reader1.sensors[id_sensor].saving_tick, unit="ns")

        previous_date = date - pd.Timedelta(reader1.tick_time*reader1.sensors[id_sensor].saving_tick, unit="ns")
        print("\r\n%d errors"%nb_errors)

def copy_data(datas, debut, fin) :
    d = {}
    for k,v in datas.items() :
        if k >= debut and k<= fin :
            d[k] = v

    return d

main()