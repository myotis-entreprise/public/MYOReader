# MyoReader
myoReader is a module able to parse the binary open format of myotis data logger and alarm station, the .myo file.

# Installation
Install from source with git:
````
git clone https://gitlab.com/myotis-entreprise/myoreader.git
````

Copy the folder `myoreader` to your own project and add the module to your python code by adding this line :
```from myoreader.myoReader import MyoReader```

Now you can create a parser an use it to parse a file :
````
reader = MyoReader()
reader.path = "my_file.myo"
reader.parse()
````

Access to information through member of the instance. All the metrix are
inside a dictionary of list. Each keys are date of the measure and 
each values are a list of measure :
````
2020/10/28 00:00:00.0000 => 25.5, 25454, 15166
````

Values are directly converted to the unit configured inside the data logger thank's to the coef parameters provided in the file.

For more examples of how to use this class, read the file "test.py" which parse a file and write metrix inside a human readable file

# File format specification
The binary file format contains 2 sections :
    * A header with the logger and sensor information
    * Data section

## header
Each field is positionned at a specific place and use a fixed number of bytes.
The only unlimited field is the station name which is a null terminated string.

The position starts from 0. The header is split into 2 sections:
* Station header
* Sensor header

Station header is fixed but the sensor header is linked of the number of sensors used in the file.

For the sampling time, all the file is relative to a "tick". 
One tick equal TICK_TIME second.
The sensor with the smallest sampling period have a sampling tick of 1. The other sensors, need to have a sampling time with a multiple of the tick 1.

### Station header

|Field|Description|Position in bytes|Size in bytes|
|---|---|---|---|
|CRC32| Cyclic redundancy check More information on (wikipedia)[https://en.wikipedia.org/wiki/Cyclic_redundancy_check]. **Currently not used**.|0|4|
|VERSION| File format version|4|2|
|STARTED_TIME| Timestamp of the beggining of the measure (:warning: not of this file)|6|8|
|SUB_SEC|Microsecond delay after STARTED_TIME|14|4|
|UID|Unique identifier of the logger|18|4|
|TICK_TIME| Time in nanosecond of a tick|22|8|
|SENSOR_COUNT| Number of sensor used|30|2|
|STATION_NAME| Name of the logger. Null terminated string.| 32|NA|


### Sensor header

These fields above are repeated for each sensors.

**As the station name is not size limited, the position of the first field is relative to the end of STATION_NAME+1**


|Field|Description|Position in bytes|Size in bytes|
|---|---|---|---|
|ID| Identification number relative to the station|0|2|
|SAVING_TICKS|Sampling period in tick unit|2|8|
|CALIB_0|Coef 0 of the calibration polynom floating represention|10|4|
|CALIB_1|Coef 1 of the calibration polynom floating represention|14|4|
|COEF_0|Coef 0 of the conversion polynom floating represention. Used to convert raw data into the unit of the sensor|18|4|
|COEF_1|Coef 1 of the conversion polynom floating represention. Used to convert raw data into the unit of the sensor|22|4|




# Licence
[MIT](https://spdx.org/licenses/MIT.html#licenseText) 
