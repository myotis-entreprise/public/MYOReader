from myoreader.myoReader import MyoReader
from myoreader import *
import datetime
from io import SEEK_SET
import matplotlib.pyplot as plt
import os

#Affichage des signaux de deux stations. Le signal de la station 1, doit être sur la voie 2
#et le signal de la station 2 doit être sur la voie 0.
#Seulent les parties communes (au niveau horraire) des signaux sont affichées afin de 
#permettre la comparaison manuelle de leur phase, amplitude, fréquence etc
#Un troisième graphique montre le psd de la station 1 pour connaitre sa répartition fréquencielle
def main() : 
    path = os.path.dirname(os.path.abspath(__file__)) + "\\Example1\\"
    
    for dir in sorted(os.listdir(path)) :
      
        reader1 = MyoReader(path+"/"+dir)
        reader1.parse()

        print("")
        print("File :\t\t\t" + dir)
        print("Format version :\t" + str(reader1.version))
        print("Station UID :\t\t" + format(reader1.uid, 'x'))
        print("Station name :\t\t" + reader1.station_name)
        print("Sequence start at :\t" + reader1.date_time.strftime("%Y/%m/%d %H:%M:%S.%f"))
        #print("File start at : " + reader1.datas[0].strftime("%Y/%m/%d %H:%M:%S.%f") + )
        print("")
        print("Position|\tName\t|\tFs\t|\tUnit\t|       Coef1        |       Coef0        |       Calib0        |       Calib1        |")
        print("---------------------------------------------------------------------------------------------------------------------------------------------------------")
        for sensor in reader1.sensors :
             print("   " + str(sensor.id) + "    |\t" + "?" + "\t|\t" + str(1000000000/(sensor.saving_tick*reader1.tick_time))  + "\t|\t"+ "?" +"\t|  "+ 
             '%.12f' % sensor.coefs[0]   + "   |   "+ '%.12f' % sensor.coefs[1]  +"   |   " + '%.12f' % sensor.calibration[0] +"   |   "+ '%.12f' % sensor.calibration[1]  + "   |")

        
        i = 1
        for sensor in reader1.sensors :
            plt.subplot(len(reader1.sensors)*2,1,i)
            y = list(sensor.datas.values())
            t = list(sensor.datas.keys())
            if len(y) == 0 :
                continue
            plt.plot(t,y, 'o-')
            plt.subplot(len(reader1.sensors)*2,1,i+1)
            plt.psd(y, Fs=(1000000000/(sensor.saving_tick*reader1.tick_time)))
            i+=2
      
        plt.show()
        break

def copy_data(datas, debut, fin) :
    d = {}
    for k,v in datas.items() :
        if k >= debut and k<= fin :
            d[k] = v

    return d

main()