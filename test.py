from myoreader.myoReader import MyoReader
from myoreader import *
import datetime
from io import SEEK_SET
import matplotlib.pyplot as plt
import os


def test1() : 
    reader = MyoReader("template.myo")

    crc = reader.parse_crc()
    version = reader.parse_version()
    startTime = reader.parse_started_time()
    uid = reader.parse_uid()
    tick_start = reader.parse_first_tick()
    tick_time = reader.parse_tick_time()
    stationName = reader.parse_station_name()
    sensors = reader.parse_sensors()
    datas = reader.parse_datas()


    assert(crc == 0)
    assert(version == 1)
    #assert(startTime.strftime("%y/%m/%d %H:%M:%S.%f") == "20/10/27 15:13:02.056342")
    assert("0x%04x" % uid == "0xeb1a045e")
    assert(tick_start == 1)
    assert(tick_time == 4000)
    assert(stationName == "netrisk v2")

    mode = "r+"
    if os.path.exists("test.csv") == False :
        mode = "w+"
    #Convert in a csv file
    f1 = open("test.csv", mode)
    for tick, points in datas.items() :
        l = tick.strftime("%Y/%m/%d %H:%M:%S.%f") + ";"
        for p in points :
            l += str(p) + ";"

        l+='\r'
        f1.write(l.replace('.',','))

    f1.seek(0, SEEK_SET)


    reader2 = MyoReader("template.myo")
    reader2.parse()
    sensors = reader.sensors
    datas = reader2.datas

    mode = "r+"
    if os.path.exists("test2.csv") == False :
        mode = "w+"
    #Convert in a csv file
    f2 = open("test2.csv", mode)
    for tick, points in datas.items() :
        l = tick.strftime("%Y/%m/%d %H:%M:%S.%f") + ";"
        for p in points :
            l += str(p) + ";"

        l+='\r'
        f2.write(l.replace('.',','))

    f2.seek(0, SEEK_SET)


    #Check if files are identicals
 
    for line1 in f1:
        line2 = f2.readline()
        if line1 !=line2:
            print("Error: " +  line1 + " vs " + line2)
            return(-1)
            
                
    f1.close()
    f2.close()

    print("Test1 OK!")




test1()


